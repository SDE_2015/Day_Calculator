class PersonagesController < ApplicationController
  before_action :set_personage, only: [:show, :edit, :update, :destroy]
  # GET /personages
  # GET /personages.json





  def index
    @personages = Personage.all
  end

  # GET /personages/1
  # GET /personages/1.json
  def show
  end

  # GET /personages/new
  def new
    @personage = Personage.new
  end

  # GET /personages/1/edit
  def edit
  end

  # POST /personages
  # POST /personages.json
  def create
    @personage = Personage.new(personage_params)

    @personage.daysalive = (Date.today() - @personage.birthday)
    @personage.martian = ((Date.today()-@personage.birthday)*0.97)

    respond_to do |format|
      if @personage.save
        format.html { redirect_to @personage, notice: 'Personage was successfully created.' }
        format.json { render :show, status: :created, location: @personage }
      else
        format.html { render :new }
        format.json { render json: @personage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /personages/1
  # PATCH/PUT /personages/1.json
  def update
    respond_to do |format|
      if @personage.update(personage_params)
        format.html { redirect_to @personage, notice: 'Personage was successfully updated.' }
        format.json { render :show, status: :ok, location: @personage }
      else
        format.html { render :edit }
        format.json { render json: @personage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /personages/1
  # DELETE /personages/1.json
  def destroy
    @personage.destroy
    respond_to do |format|
      format.html { redirect_to personages_url, notice: 'Personage was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_personage
      @personage = Personage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def personage_params
      params.require(:personage).permit(:name, :lastname, :birthday, :daysalive)
    end
end
