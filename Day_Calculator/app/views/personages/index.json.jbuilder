json.array!(@personages) do |personage|
  json.extract! personage, :id, :name, :birthday, :daysalive
  json.url personage_url(personage, format: :json)
end
