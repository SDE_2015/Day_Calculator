require 'test_helper'

class PersonagesControllerTest < ActionController::TestCase
  setup do
    @personage = personages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:personages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create personage" do
    assert_difference('Personage.count') do
      post :create, personage: { birthday: @personage.birthday, daysalive: @personage.daysalive, name: @personage.name }
    end

    assert_redirected_to personage_path(assigns(:personage))
  end

  test "should show personage" do
    get :show, id: @personage
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @personage
    assert_response :success
  end

  test "should update personage" do
    patch :update, id: @personage, personage: { birthday: @personage.birthday, daysalive: @personage.daysalive, name: @personage.name }
    assert_redirected_to personage_path(assigns(:personage))
  end

  test "should destroy personage" do
    assert_difference('Personage.count', -1) do
      delete :destroy, id: @personage
    end

    assert_redirected_to personages_path
  end
end
