class CreatePersonages < ActiveRecord::Migration
  def change
    create_table :personages do |t|
      t.string :name
      t.date :birthday
      t.integer :daysalive

      t.timestamps null: false
    end
  end
end
